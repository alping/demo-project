export default defineNuxtConfig({
  telemetry: false,
  devtools: { enabled: true },
  modules: [
    '@unocss/nuxt',
  ],
  css: [
    '@unocss/reset/tailwind.css',
  ],
  app: {
    head: {
      viewport: 'width=device-width,initial-scale=1',
      link: [
        { rel: 'icon', href: '/favicon.ico', sizes: 'any' },
      ],
      meta: [
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { name: 'description', content: 'Demo project' },
      ],
    },
  },
  runtimeConfig: {
    public: {
      apiBase: 'http://localhost:8000',
    }
  },
  features: {
    // For UnoCSS
    inlineStyles: false,
  },
})
