# Demo Project - App

Simple display and visualization of the data from the API.

```shell
# Install bun
curl -fsSL https://bun.sh/install | bash
# Source .bashrc, change path as necessary
source /home/gitpod/.bashrc
# Install dependencies
cd app
bun install
# Run dev server
bun run dev  # port 3000
```

The web app can be accessed at [http://localhost:3000/](http://localhost:3000/).
