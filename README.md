# Demo Project

A demo data pipeline and visualization project.

![Schematic of demo project](assets/demo-project.svg)

## Run with Docker

```shell
docker compose up --build
```

The following containers will build and run:

- [Data generation](server/data_generation/README.md) runs only once to generate the source data
- [Data pipeline](server/data_pipeline/README.md) -> http://localhost:5000/
- [Data API](server/data_api/README.md) -> http://localhost:8000/ and http://localhost:8000/docs
- [App](app/README.md) -> http://localhost:3000/

(Go to each subprojects README, linked above, for more information)

To process the data used for the API and displayed on the website, do the following from the [Dagster UI](http://localhost:5000/):

1. Go to `Deployment` in the top menu
2. Click on the tab for `Daemons`
3. Toggle the `Auto-materializing` daemon to on
4. Go to `Assets` in the top menu
5. Click on `View global asset lineage` in the top right corner
6. Click on the `ETL` group to expand it
7. Right click on `extract` and choose `Materialize`
8. Click `Launch backfill`
9. The data processing steps will now run and when they are finished the data will be available for App through the API

## Local Setup

```shell
# Server
python -m venv .venv
source .venv/bin/activate
pip install -e "server/data_generation[dev]"
pip install -e "server/data_pipeline[dev]"
pip install -e "server/data_api[dev]"

# App
curl -fsSL https://bun.sh/install | bash
source /home/gitpod/.bashrc  # Change path as needed
cd app
bun install
```

### Tests

Run tests using pytests:

```shell
pytest
```

If there is no database, the test for the API will fail as it can not send any data.

### Data generation

Generate data for the data pipeline with:

```shell
datagen
```

### Data pipeline

Run Dagster UI with:

```shell
cd server/data_pipeline
dagster dev -p 5000  # port 5000
```

And visit it at http://localhost:5000/

### Data API

Run FastAPI with:

```shell
uvicorn data_api.api:app --reload  # port 8000
```

And visit it at http://localhost:8000/ and http://localhost:8000/docs

### App

Run Nuxt with:

```shell
bun run dev  # port 3000
```

And visit it at http://localhost:3000/

## Pin requirements

Pin requirements for production:

```shell
(cd server/data_generation; pip-compile -U)
(cd server/data_pipeline; pip-compile -U)
(cd server/data_api; pip-compile -U)
```

## Environmental variables

Set environmental variables globally or in `.env` files (change paths as needed):

```text
DATA_PATH=/workspace/demo-project/data
DATABASE_PATH=/workspace/demo-project/data/database.db
DAGSTER_HOME=/workspace/demo-project/server/data_pipeline/.dagster
```

## Technologies used

- [Numpy](https://numpy.org/) for generating the simulated data
- [Pandas](https://pandas.pydata.org/) for some dataframe operations
- [Polars](https://pola.rs/) as the main dataframe package
- [lifelines](https://lifelines.readthedocs.io/en/latest/) for Kaplan-Meier estimation
- [Dagster](https://dagster.io/) for workflow orchestration
- [DuckDB](https://duckdb.org/) for fast persistent data storage
- [FastAPI](https://fastapi.tiangolo.com/) for creating the API
- [Bun](https://bun.sh/) as a faster alternative to npm
- [Vue 3](https://nuxt.com/) for creating web UI components
- [Nuxt 3](https://vuejs.org/) as the meta framework for the app
- [UnoCSS](https://unocss.dev/) for CSS styling with utility classes
- [Observable Plot](https://observablehq.com/plot/) for plotting

## Screenshots

![Screenshot of Dagster UI](assets/screenshot-dagster.png)

---

![Screenshot of API](assets/screenshot-api.png)

---

![Screenshot of app](assets/screenshot-app.png)
