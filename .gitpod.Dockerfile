FROM gitpod/workspace-full

RUN pyenv install 3.11 \
    && pyenv global 3.11

RUN curl -fsSL https://bun.sh/install | bash
