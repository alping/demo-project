# Demo Project - Data API

Data API using FastAPI.

```shell
pip install -e "server/data_api[dev]"
uvicorn data_api.api:app --reload
```

The API can be accessed at [http://localhost:8000/](http://localhost:8000/), with documentation at [http://localhost:8000/docs](http://localhost:8000/docs).

The output data directory can be specified with an environment variable or in an `.env` file.

```text
DATABASE_PATH=/workspace/demo-project/data/database.db
```
