from fastapi.testclient import TestClient
import pytest
from data_api.api import app

@pytest.fixture()
def app_client():
    yield TestClient(app)

def test_api_endpoints(app_client):
    assert app_client.get("/").status_code == 200
    assert app_client.get("/overview").status_code == 200
    assert app_client.get("/summary").status_code == 200
    assert app_client.get("/kaplan-meier/table").status_code == 200
    assert app_client.get("/kaplan-meier/estimates").status_code == 200

# TODO: More extensive tests for the API and tests for the database interface
