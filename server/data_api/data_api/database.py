from pathlib import Path
import typing

import duckdb
from lifelines import KaplanMeierFitter
import pandas as pd
import polars as pl
import polars.selectors as cs


class Database:
    def __init__(self, database_path: str | Path) -> None:
        """Initiate the database interface

        Args:
            database_path (str | Path): The path to the database
        """

        database_path = Path(database_path)
        self.database_path = database_path

    def _load_data(self) -> pl.DataFrame:
        """Internal method for loading fresh data from the DuckDB database

        Returns:
            pl.DataFrame: The data as a Polars DataFrame
        """

        # Connect to the database with a context manager (to make sure it closes) and
        # load the data into a Polars DataFrame
        with duckdb.connect(self.database_path.as_posix()) as conn:
            data = conn.sql(
                """
                SELECT * FROM data_table
                """
            ).pl()

        # Make sure that the data has the correct formatting
        formatted_data = data.with_columns(pl.col(pl.String).cast(pl.Categorical))

        return formatted_data

    def get_overview(self) -> dict[str, typing.Any]:
        """Get overview statistics of the data

        Returns:
            dict[str, typing.Any]: Overview statistics
        """
        # Load the data to make sure it is up to date
        data = self._load_data()

        number_of_observations = data.height

        # Remove the ID number from the number of variables
        number_of_variables = data.width - 1

        proportion_with_missing = (
            data.select(pl.all().is_null()).max_horizontal().mean()
        )

        return {
            "number_of_observations": number_of_observations,
            "number_of_variables": number_of_variables,
            "proportion_with_missing": proportion_with_missing,
        }

    def get_summary(self) -> dict[typing.Hashable, typing.Any]:
        """Get summary statistics for the data that can be presented in a table

        Returns:
            dict[typing.Hashable, typing.Any]: Summary statistics
        """

        summary = (
            # Load the data to make sure it is up to date
            self._load_data()
            # The ID number should not be included in the summary
            .select(pl.exclude("pid"))
            # Categorical variables need to be in dummy variable form
            .to_dummies(cs.categorical())
            # Boolean variables need to be converted to integers
            .with_columns(cs.boolean().cast(int))
            # Use the built in describe method
            .describe()
            # Transpose the data for easier use in a table in the frontend
            .transpose(
                include_header=True, header_name="variable", column_names="statistic"
            )
            # Pick only a subset of the statistics
            .select("variable", missing="null_count", mean="mean", std="std")
            # Convert to Pandas to be able to use more feature-rich conversion to dict
            .to_pandas()
            # Setting of the index and renaming the column axis makes to_dict("tight")
            # create data that more easily can be used in a table in the frontend
            .set_index("variable")
            .rename_axis(columns="statistic")
            .to_dict("tight")
        )

        return summary

    def get_kaplan_meier_data(
        self,
    ) -> tuple[
        list[dict[typing.Hashable, typing.Any]], list[dict[typing.Hashable, typing.Any]]
    ]:
        """Get the Kaplan-Meier estimates for the survival curve, stratified on sex (male/female)

        Returns:
            tuple[ list[dict[typing.Hashable, typing.Any]], list[dict[typing.Hashable, typing.Any]] ]: Table, Estimates
        """
        # Load the data to make sure it is up to date
        data = self._load_data()

        # The plot is slow to render on the frontend with so many observations, so we
        # make an approximation with a random sample. A better approximation would be
        # more suitable or alternatively a faster render method for the plot, e.g.
        # Canvas instead of SVG. Set seed to take the same sample every time (for the
        # same data).
        data_sample = data.sample(n=min(2000, data.height), seed=1234)

        # Split the data into male and female data
        male_data = data_sample.filter(pl.col("sex").eq("male")).select("time", "event")
        female_data = data_sample.filter(pl.col("sex").eq("female")).select(
            "time", "event"
        )

        # Initiate the Kaplan-Meier fitter
        kmf = KaplanMeierFitter()

        # Fit the male data and get the estimates and the table
        male_fit = kmf.fit(male_data["time"], male_data["event"], label="male")
        male_estimates: pd.DataFrame = male_fit.survival_function_.join(
            male_fit.confidence_interval_
        )
        male_table: pd.DataFrame = male_fit.event_table

        # Fit the female data and get the estimates and the table
        female_fit = kmf.fit(female_data["time"], female_data["event"], label="female")
        female_estimates: pd.DataFrame = female_fit.survival_function_.join(
            female_fit.confidence_interval_
        )
        female_table: pd.DataFrame = female_fit.event_table

        # The Kaplan-Meier fitter returns a Pandas dataframe, so we continue in Pandas
        # here. Not sure about the performance implications of converting to Polars and
        # using that instead.

        # Combine the estimates for males and females into a single dataframe
        combined_estimates = (
            pd.concat(
                [
                    male_estimates.rename(
                        columns={
                            "male": "estimate",
                            "male_lower_0.95": "lower",
                            "male_upper_0.95": "upper",
                        }
                    ).assign(sex="male"),
                    female_estimates.rename(
                        columns={
                            "female": "estimate",
                            "female_lower_0.95": "lower",
                            "female_upper_0.95": "upper",
                        }
                    ).assign(sex="female"),
                ]
            )
            .rename_axis(index="time")
            .reset_index()
            .sort_index()
        )

        # Combine the tables for males and females into a single dataframe
        combined_tables = (
            pd.concat(
                [
                    male_table.assign(sex="male"),
                    female_table.assign(sex="female"),
                ]
            )
            .rename_axis(index="time")
            .reset_index()
            .sort_index()
        )

        # Return both the table and estimates
        return combined_tables.to_dict("records"), combined_estimates.to_dict("records")
