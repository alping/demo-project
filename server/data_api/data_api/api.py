import os
from pathlib import Path
from typing import Literal
import typing

from fastapi import FastAPI, HTTPException
from dotenv import load_dotenv

from data_api.database import Database

load_dotenv()

# Path to database from environment variable
DATABASE_PATH: Path = Path(os.getenv("DATABASE_PATH") or "").resolve()

# Initiate the API and the interface to the database
app = FastAPI()
db = Database(DATABASE_PATH)


def check_db_exists():
    if not DATABASE_PATH.is_file():
        raise HTTPException(status_code=400, detail="Database does not exist")


@app.get("/")
def read_root() -> dict[str, dict[str, str]]:
    """Root if the API returns a list of available API endpoints

    Returns:
        dict[str, dict[str, str]]: Available API endpoints
    """
    check_db_exists()

    return dict(
        endpoints={
            "/overview": "Overview of data",
            "/summary": "Summary of data",
            "/kaplan-meier/{value_type}": "Kaplan-Meier estimate of survival curve",
        }
    )


@app.get("/overview")
def overview() -> dict[str, typing.Any]:
    """Returns an overview of the data

    Returns:
        dict[str, typing.Any]: Overview of the data
    """
    check_db_exists()

    return db.get_overview()


# FastAPI docs (openapi.json) crashes when providing typing
# -> dict[typing.Hashable, typing.Any]
@app.get("/summary")
def summary():
    """Returns summary statistics for the data

    Returns:
        dict[typing.Hashable, typing.Any]: Summary of the data
    """
    check_db_exists()

    return db.get_summary()


# FastAPI docs (openapi.json) crashes when providing typing
# -> list[dict[typing.Hashable, typing.Any]]
@app.get("/kaplan-meier/{value_type}")
def kaplan_meier(value_type: Literal["table", "estimates"]):
    """Returns the Kaplan-Meier estimate of the survival function for the data

    Args:
        value_type (Literal["table", "estimates"]): The type of data to be returned

    Raises:
        HTTPException: value_type needs to be "table" or "estimates"

    Returns:
        list[dict[typing.Hashable, typing.Any]]: The Kaplan-Meier data as table or estimates
    """
    check_db_exists()

    # If value_type is not "table" or "estimates", raise an error
    if value_type not in ["table", "estimates"]:
        raise HTTPException(
            status_code=400, detail="value_type must be 'table' or 'estimates'"
        )

    # table is currently not used in the frontend (and it doesn't really make sense to
    # calculate both of them and then only return one).
    table, estimates = db.get_kaplan_meier_data()

    # Return the type of data requested
    match value_type:
        case "table":
            return table
        case "estimates":
            return estimates
