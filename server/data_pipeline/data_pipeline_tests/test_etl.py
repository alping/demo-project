from pathlib import Path

import duckdb
import polars as pl

from data_pipeline.data_spec import data_type
from data_pipeline.etl import extract_data, transform_data, load_data


def test_extract_data():
    """Test the data extraction"""

    # Create a dummy data specification
    dummy_data_spec: dict[str, dict[str, data_type]] = dict(
        dummy_data={
            "a": int,
            "b": float,
            "c": bool,
            "d": str,
            "e": pl.Categorical,
        }
    )

    # Create dummy data following the specification
    dummy_data = pl.DataFrame(
        dict(
            a=pl.Series([0, 1, 2, 3, 4], dtype=pl.Int64),
            b=pl.Series([0.1, 0.2, 0.3, 0.4, 0.5], dtype=pl.Float64),
            c=pl.Series([True, None, True, False, True], dtype=pl.Boolean),
            d=pl.Series(["u", "v", "x", "y", "z"], dtype=pl.String),
            e=pl.Series(["g", "h", "g", None, "g"], dtype=pl.Categorical),
        )
    )

    # Write the dummy data to a temp file
    temp_csv_file = Path(".temp-csv-for-testing-LXCCCOFPPM.csv")
    dummy_data.write_csv(temp_csv_file)

    # Run the tested function
    extracted_data = extract_data(temp_csv_file, dummy_data_spec["dummy_data"])

    # Make sure that the extracted data match the original data
    assert extracted_data.equals(
        dummy_data
    ), "The extracted data does not match the original data"

    # Delete the temp CSV file
    temp_csv_file.unlink(missing_ok=True)


def test_transform_data():
    """Test the data transform"""

    # Create the expected dummy datasets
    dummy_base_data = pl.DataFrame(
        dict(pid=range(20), base_value=pl.Series(range(20)) * 10)
    )

    dummy_socioeconomic_data = pl.DataFrame(
        dict(pid=range(15), socioeconomic_value=pl.Series(range(15)) * 100)
    )

    dummy_time_to_event_data = pl.DataFrame(
        dict(pid=range(18), time_to_event_value=pl.Series(range(18)) * 1000)
    )

    # Put the datasets together in a dict to deliver to the transform_data function
    data = {
        "base_data": dummy_base_data,
        "socioeconomic_data": dummy_socioeconomic_data,
        "time_to_event_data": dummy_time_to_event_data,
    }

    # Run the tested function
    transformed_data = transform_data(data)

    # Make sure that no subjects have been lost in the transform
    assert (
        transformed_data.height == dummy_base_data.height
    ), "Observations have been lost during transform"


def test_load_data():
    """Test the data loading"""

    # Create dummy data
    dummy_data = pl.DataFrame(
        dict(
            a=[0, 1, 2, 3, 4],
            b=[5, 6, 7, 8, 9],
        )
    )

    # Specify a temp database file
    temp_database_file = Path(".temp-database-for-testing-LXCCCOFPPM.db")

    # Run the tested function
    load_data(dummy_data, temp_database_file)

    # Read the resulting database
    with duckdb.connect(temp_database_file.as_posix()) as conn:
        loaded_data = conn.sql(
            """
            SELECT * FROM data_table
            """
        ).pl()

    # Make sure that the loaded data match the original data
    assert loaded_data.equals(
        dummy_data
    ), "The loaded data does not match the original data"

    # Delete the temp database file
    temp_database_file.unlink(missing_ok=True)
