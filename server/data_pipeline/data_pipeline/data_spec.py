from polars import Categorical

# Specification for the columns and data types expected in the data

data_type = type[int | float | bool | str | Categorical]

data_spec: dict[str, dict[str, data_type]] = dict(
    base_data={
        "pid": int,
        "age_y": float,
        "sex": Categorical,
        "region": Categorical,
    },
    socioeconomic_data={
        "pid": int,
        "highest_education": Categorical,
        "last_years_salary": float,
    },
    time_to_event_data={
        "pid": int,
        "time": float,
        "event": bool,
    },
)
