import os
from pathlib import Path
from dotenv import load_dotenv

load_dotenv()

# Path to data from environment variable
DATA_PATH: Path = Path(os.getenv("DATA_PATH") or "").resolve()

# Path to database from environment variable
DATABASE_PATH: Path = Path(os.getenv("DATABASE_PATH") or "").resolve()
