from pathlib import Path
import warnings

import polars as pl
from dagster import (
    asset,
    AssetDep,
    AssetExecutionContext,
    AssetIn,
    AssetKey,
    AutoMaterializePolicy,
    ExperimentalWarning,
    IdentityPartitionMapping,
    SourceAsset,
    SpecificPartitionsPartitionMapping,
    StaticPartitionsDefinition,
)

from data_pipeline.utils import DATA_PATH, DATABASE_PATH
from data_pipeline.data_spec import data_spec
from data_pipeline.etl import extract_data, load_data, transform_data

# Disable experimental warnings for Dagster, needed for AutoMaterializePolicy
warnings.filterwarnings("ignore", category=ExperimentalWarning)

# Get the 'keys' from the data specification
data_keys = list(data_spec.keys())

# Define a partition definition based on these keys, used for the data extraction
data_partition_def = StaticPartitionsDefinition(data_keys)

# A source asset describing the raw data files not generated in the data pipeline, uses
# the partition definition defined above
data_storage = SourceAsset(
    key=AssetKey("data_storage"),
    description="Raw data files delivered from data source.",
    partitions_def=data_partition_def,
    group_name="DATA",
)


# An asset for the extracted data, uses the same partition definition as the source
# data. It runs the extraction for each partition, i.e. for each CSV file with raw data.
# This asset depends on the raw data
@asset(
    description="Extract the data",
    partitions_def=data_partition_def,
    deps=[
        AssetDep(
            data_storage,
            partition_mapping=IdentityPartitionMapping(),
        )
    ],
    io_manager_key="parquet_io_manager",
    compute_kind="polars",
    group_name="ETL",
)
def extract(context: AssetExecutionContext) -> pl.DataFrame:
    """Asset for extracting the raw data

    Args:
        context (AssetExecutionContext): Used for identifying the CSV file to be extracted

    Returns:
        pl.DataFrame: The extracted data
    """
    # Get the partition key from the asset execution context
    part_key = context.asset_partition_key_for_output()

    # Specify the path to the CSV file using DATA_PATH and the partition key
    csv_file = DATA_PATH / Path(part_key).with_suffix(".csv")
    # Get the correct data specification using the partition key
    spec = data_spec[part_key]

    # Extracts the data from the data source and apply correct formatting
    extracted_data = extract_data(csv_file, spec)

    return extracted_data


# An asset for the transformed data, depends on all the partitions of the extract asset.
# Should automatically run when the dependencies are updated, but this functionality is
# still 'unstable' and is currently not functioning properly
@asset(
    description="Transform the data",
    ins={
        "data_frames": AssetIn(
            "extract", partition_mapping=SpecificPartitionsPartitionMapping(data_keys)
        )
    },
    auto_materialize_policy=AutoMaterializePolicy.eager(),
    io_manager_key="parquet_io_manager",
    compute_kind="polars",
    group_name="ETL",
)
def transform(data_frames: dict[str, pl.DataFrame]) -> pl.DataFrame:
    """Asset for transforming the extracted data

    Args:
        data_frames (dict[str, pl.DataFrame]): The extracted dataframes (partitioned)

    Returns:
        pl.DataFrame: The transformed/merged dataframe
    """
    # Merges the separate data into a single dataframe
    transformed_data = transform_data(data_frames)
    return transformed_data


# An asset for the loaded data, depends on the transformed asset. Should automatically
# run when the dependencies are updated, but this functionality is still 'unstable' and
# is currently not functioning properly
@asset(
    description="Load the data",
    auto_materialize_policy=AutoMaterializePolicy.eager(),
    io_manager_key="parquet_io_manager",
    compute_kind="duckdb",
    group_name="ETL",
)
def load(transform: pl.DataFrame) -> None:
    # Loads the data into persisted DuckDB database
    load_data(transform, DATABASE_PATH)

    # Returns nothing as the data is modified directly in the database
