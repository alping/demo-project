from pathlib import Path
import duckdb
import polars as pl
from data_pipeline.data_spec import data_type


def extract_data(csv_file: str | Path, spec: dict[str, data_type]) -> pl.DataFrame:
    """Extract the data from the data source and apply correct formatting

    Args:
        csv_file (str | Path): Path to CSV file to be extracted
        spec (dict[str, data_type]): column: data_type, the data specification to use for formatting

    Returns:
        pl.DataFrame: The extracted data as a Polars DataFrame
    """

    # Make sure csv_file is a Path and exists
    csv_file = Path(csv_file)
    assert csv_file.is_file(), f"File '{csv_file}' does not exist"

    # TODO: Assert that spec follows the correct specification

    # Read the data and convert the columns into the correct dtypes according to the
    # specification. Also replace any NaN values with proper a missing indicator.
    formatted_data = (
        # Read the data
        pl.read_csv(csv_file)
        # Cast the columns to correct types according to spec
        .with_columns([pl.col(column).cast(dtype) for column, dtype in spec.items()])
        # Replace NaN values with proper missing
        .fill_nan(None)
    )

    return formatted_data


def transform_data(data: dict[str, pl.DataFrame]) -> pl.DataFrame:
    """Merge the separate data into a single dataframe

    Args:
        data (dict[str, pl.DataFrame]): Name-DataFrame pairs for the data to be merged

    Returns:
        pl.DataFrame: The merged data in a Polars DataFrame
    """
    # Use the base_data as the base and join the other dataframes to it, making sure
    # that no subjects are lost due to missing data (by using a left join)
    merged_data = (
        data["base_data"]
        .join(data["socioeconomic_data"], on="pid", how="left")
        .join(data["time_to_event_data"], on="pid", how="left")
    )

    return merged_data


def load_data(data: pl.DataFrame, database_file: str | Path) -> None:
    """Load the data into persisted DuckDB database

    Args:
        data (pl.DataFrame): The data to be loaded
        database_file (str | Path): The path to the database (does not need to exist)
    """

    # Make sure that database_file is a Path
    database_file = Path(database_file)

    # Make sure that the path to database_file exist, if not create it
    database_file.parent.mkdir(parents=True, exist_ok=True)

    # Connect to the database and create or replace the data_table with the new data.
    # Using a context manager ensures that the database connection is closed
    with duckdb.connect(database_file.as_posix()) as conn:
        conn.sql(
            """
            CREATE OR REPLACE TABLE data_table AS
                SELECT *
                FROM data
            """
        )
