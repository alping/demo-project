# %%
from dagster import (
    UPathIOManager,
    Definitions,
    InputContext,
    load_assets_from_modules,
    OutputContext,
)
import polars as pl
from upath import UPath

# %%

from data_pipeline.utils import DATA_PATH
from . import assets


class PolarsParquetIOManager(UPathIOManager):
    """IO Manager for handling reading and writing of data with Polars"""

    extension: str = ".parquet"

    def dump_to_path(self, context: OutputContext, obj: pl.DataFrame, path: UPath):
        """Handle writing data as output"""
        obj.write_parquet(path)

    def load_from_path(self, context: InputContext, path: UPath) -> pl.DataFrame:
        """Handle loading the data as input"""
        return pl.read_parquet(path)


# Make Dagster aware of the assets and resources

all_assets = load_assets_from_modules([assets])

defs = Definitions(
    assets=all_assets,
    resources={
        "parquet_io_manager": PolarsParquetIOManager(base_path=UPath(DATA_PATH))
    },
)
