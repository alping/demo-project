# Demo Project - Data Pipeline

A data pipeline for extracting, transforming and loading the data in the demo project.
Built with [Dagster](https://dagster.io/).

1. Extract: Read the data and convert the columns into the correct dtypes according to a specification. Also replace any NaN values with proper a missing indicator
2. Transform: Merge the separate data into a single dataframe
3. Load: Load the data into persisted DuckDB database

```shell
# Install dependencies
pip install -e "server/data_pipeline[dev]"
# Start Dagster daemon and web UI
cd server/data_pipeline
dagster dev -p 5000
```

The Dagster UI can be accessed at [http://localhost:5000/](http://localhost:5000/).
To start the data pipeline, do the following:

1. Go to `Deployment` in the top menu
2. Click on the tab for Daemons
3. Toggle the `Auto-materializing` daemon to on
4. Go to `Assets` in the top menu
5. Click on `View global asset lineage` in the top right corner
6. Click on the `ETL` group to expand it
7. Right click on `extract` and choose `Materialize`
8. Click `Launch backfill`
9. The data processing steps will now run

The output data directory and the database file can be specified with environment variables or in an `.env` file.

```text
DATA_PATH=/workspace/demo-project/data
DATABASE_PATH=/workspace/demo-project/data/database.db
```
