import polars as pl

from data_generation.generate_data import generate_data


def test_generate_data():
    """Test the data generation"""

    # Get the data
    data = generate_data()

    # Test that all 3 datasets were generated
    assert len(data) == 3, "Not all datasets were generated"

    # For each dataset test that it is a Polars DataFrame with some rows and columns
    for name, data_frame in data.items():
        assert isinstance(
            data_frame, pl.DataFrame
        ), f"'{name}' is not a Polars DataFrame"

        assert data_frame.width > 0, f"DataFrame '{name}' has no columns"
        assert data_frame.height > 0, f"DataFrame '{name}' has no rows"
