import os
from pathlib import Path

from dotenv import load_dotenv
import numpy as np
import polars as pl

load_dotenv()

# Path to data from environment variable
DATA_PATH: Path = Path(os.getenv("DATA_PATH") or "").resolve()

# Initiate random state (with seed for reproducibility only when testing)
# rng = np.random.RandomState(1234)
rng = np.random.RandomState()


def generate_data(
    data_dir_path: str | Path | None = None, sample_size: int | None = None
) -> dict[str, pl.DataFrame]:
    """Generate the data needed for the rest of the demo project


    Args:
        data_dir_path (str | Path | None, optional): Path for writing the data. Defaults to None.
        sample_size (int | None, optional): Size of the generated population. Defaults to None.

    Returns:
        dict[str, pl.DataFrame]: Name-DataFrame pairs of the data generated
    """

    # If sample size is not specified, set it to a random value in a range
    if sample_size is None:
        sample_size = rng.randint(7_000, 10_000)

    # The id numbers for the subject in the population
    personal_id = np.arange(sample_size)

    # Simulate base data for each subject
    base_data = pl.DataFrame(
        dict(
            # The personal ID number
            pid=personal_id,
            # Age from a normal distribution clipped to a reasonable interval
            age_y=np.clip(rng.normal(50, 20, sample_size), 0, 110),
            # Biological sex
            sex=rng.choice(["male", "female"], sample_size),
            # Region in Sweden
            region=rng.choice(["north", "middle", "south"], sample_size),
        )
    )

    # Simulate socioeconomic data for each subject, has missing
    socioeconomic_data = pl.DataFrame(
        dict(
            # The personal ID number
            pid=personal_id,
            # Highest achieved education
            highest_education=rng.choice(
                ["primary_school", "high_school", "university"],
                sample_size,
                p=[0.1, 0.6, 0.3],
            ),
            # Salary for last year from a normal distribution clipped at a lowest value,
            # this variable has 10% missing values
            last_years_salary=np.where(
                rng.binomial(1, 0.1, sample_size),
                np.nan,
                np.clip(rng.normal(34_000, 10_000, sample_size), 10_000, None),
            ),
        )
    )

    # Simulate time-to-event data: Event and censoring times from separate exponential
    # distributions, with average time-to-event different for males and females
    event_time = rng.exponential(
        base_data.select(
            pl.when(pl.col("sex").eq("male")).then(4).otherwise(3)
        ).to_series()
    )
    censoring_time = rng.exponential(2, sample_size)

    # There has been an observed event if the event_time is shorter than the
    # censoring_time
    event = event_time < censoring_time

    # Put together time-to-event data into a DataFrame
    time_to_event_data = pl.DataFrame(
        dict(
            # The personal ID number
            pid=personal_id,
            # The time-to-event or censoring time
            time=np.where(event, event_time, censoring_time),
            # An indicator if the event was observed or not
            event=event,
        )
    )

    # If there is a data_dir_path specified, write the separate datasets to this
    # directory
    if data_dir_path is not None:
        # Make sure data_dir_path is a Path
        data_dir_path = Path(data_dir_path)

        # If data_dir_path does not exist, create it including parent directories
        data_dir_path.mkdir(parents=True, exist_ok=True)

        # Export the data as CSV files
        base_data.write_csv(data_dir_path / "base_data.csv")
        socioeconomic_data.write_csv(data_dir_path / "socioeconomic_data.csv")
        time_to_event_data.write_csv(data_dir_path / "time_to_event_data.csv")

    # Also return the dataframes, useful for testing
    return {
        "base_data": base_data,
        "socioeconomic_data": socioeconomic_data,
        "time_to_event_data": time_to_event_data,
    }


# Used when specifying the CLI command for generating data and if the file is run as a
# standalone script
def generate_data_command():
    """Generate simulated data"""
    generated_data = generate_data(DATA_PATH)
    print(
        *[f"{name}: {'|'.join(data.columns)}" for name, data in generated_data.items()],
        sep="\n",
    )


# If running the code as a standalone script
if __name__ == "__main__":
    generate_data_command()
