# Demo Project - Data Generation

Generates data for the data pipeline in the demo project.
Can be run with the `datagen` CLI command.

```shell
# Install dependencies
pip install -e "server/data_generation[dev]"
# Generate data
datagen
```

The output data directory can be specified with an environment variable or in an `.env` file.

```text
DATA_PATH=/workspace/demo-project/data
```
